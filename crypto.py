import hashlib
import ecdsa

from segwit_addr import *

from typing import Iterable, List, Optional, Tuple, Union

# essentially following: https://en.bitcoin.it/wiki/Bech32
# 1- get pair public/private key wiht the Elliptic Curve Digital Signature Algorithm (ECDSA) using secp256k1 parameters:
#  using: https://pypi.org/project/ecdsa/
# 2- compressed the public key
#  source: from source like this one [se](https://crypto.stackexchange.com/questions/8914) i understand what is called
#  compressed key and digging more into that doc of the library i found the settings for it. see "uncompressed and
#  compressed format (defined in X9.62 and SEC1 standards)"
# 3- Perform RIPEMD-160 hashing on the result of SHA-256
# 4- conversion from 8-bits base to 5 bit base....
# 5- add witness byte HEREHEREHERE

# SOURCES
# - a bit of mathematical details about ECDSA signing: https://ebrary.net/7941/education/signing_bitcoin_transaction_using_ecdsa
# - an example of transaction with a pkscript and sigscript details: https://www.blockchain.com/btc/tx/EDA80AD83A974ECE1A09E9252D33C7E3BC4CFEAB24C5BC3B395D3502374AAFB9
# - a python lib for ecdsa: https://pypi.org/project/ecdsa/
# - stackoverflow discussion about building a bech32 addresses and link to a python implementation: https://stackoverflow.com/questions/70449756/constructing-bech32-addresses
# - my reference so far to implement the bech32 code: https://en.bitcoin.it/wiki/Bech32

# sk = ecdsa.SigningKey.generate(curve=ecdsa.SECP256k1)
# print('secret key=', sk.to_string().hex())
# print('verifying key=', sk.verifying_key.to_string().hex())

sk_hex = '4084605d4e227023687861f145210137b5927cc930badf6f040ece3007582849' # 64 hex-chars = 256 bits
vk_hex = '621c2a18e7f1dfe70639b0a401f9fa6711e16566f0d58183c87a09003c2202bff7f63b13f7fc388bc41cf2f389804682c6e4cc9f6945123089917669bd9ec737'
# 128 hex-chars = 512 bits
sk = ecdsa.SigningKey.from_string(bytearray.fromhex(sk_hex), curve=ecdsa.SECP256k1)
# vk = ecdsa.VerifyingKey.from_string(bytearray.fromhex(vk_hex), curve=ecdsa.SECP256k1)
vk = sk.verifying_key

print('secret key=', sk.to_string().hex())
print('verifying key=', vk.to_string().hex())
print('verifying key=', vk.to_string("compressed").hex(), ' (compressed)')

vk_comp_hex = '03621c2a18e7f1dfe70639b0a401f9fa6711e16566f0d58183c87a09003c2202bf' # 03 + 64 hex-chars = 256 bits = 32b

vk = ecdsa.VerifyingKey.from_string(bytearray.fromhex(vk_comp_hex), curve=ecdsa.SECP256k1)
# print('verifying key=', vk.to_string().hex(), ' (normal)')
# print('verifying key=', vk.to_string("uncompressed").hex(), ' (uncompressed)')


vk_demo_comp_hex = '0279be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798'
step01_sha256 = hashlib.new('sha256', bytearray.fromhex(vk_demo_comp_hex))
# below an optimized version of above but less consistent with the following code
# step01_sha256 = hashlib.sha256(bytearray.fromhex(vk_demo_comp_hex))
print('sha256:', step01_sha256.hexdigest())

# RIPEMD-160
step02_ripemd160 = hashlib.new('ripemd160', step01_sha256.digest())
print('ripemd160=', step02_ripemd160.hexdigest())

# below is a experiment to convert a binary string to int.
# s = '01110 10100 01111 00111 01101 11010 00000 11001 10010 00110 01011 01101 01000 10101 00100 10100 00011 10001 00010 11101 00011 01100 11101 00011 00100 01111 11000 10100 00110 01110 11110 10110'
# for n in s.split(' '):
#     print(n, int(n, base=2))

'   01110 10100 01111 00111 01101 11010 00000 11001 10010 00110 01011 01101 01000 10101 00100 10100 00011 10001 00010 11101 00011 01100 11101 00011 00100 01111 11000 10100 00110 01110 11110 10110'
'00 0e14  0f07  0d1a  0019  1206  0b0d 0815 0414 0311 021d 030c 1d03 040f 1814 060e 1e16'

def chunkstring(string, length):
    return [string[0+i:length+i] for i in range(0, len(string), length)]


# debug_list_chunked = [];
# for n in chunkstring(step02_ripemd160.hexdigest(), 2):
#     debug_list_chunked.append(n)
debug_list_chunked = chunkstring(step02_ripemd160.hexdigest(), 2)

print('chunked ripemd160 digest:', debug_list_chunked)

int_list_8based = []
for n in chunkstring(step02_ripemd160.hexdigest(), 2):
    int_list_8based.append(int(n, base=16))
    # print(n, int(n, base=16))

# here is the
print('in list 8-based:', int_list_8based)

int_list_5based = convertbits(int_list_8based, 8, 5)
print('int list 5-based:', int_list_5based)

new_hex = ''
for n in int_list_5based:
    new_hex += format(n, 'x')


print(new_hex)
new_hex = '00' + new_hex
print(new_hex)

# encoded_chk = [(chk >> 5 * (5 - i)) & 31 for i in range(6)]
#
# print('checksum:', encoded_chk)
#
# encoded_chk_hex = ''
# for n in encoded_chk:
#     encoded_chk_hex += format(n, 'x')
#
# print('checksum_hex:', encoded_chk_hex)

# call encode:
# arguments:
# - hrp: human readable part: 'bc' for MainNet and 'tb' for TestNet
# - version: 0 for BECH32 1 for BECH32M
# - data: list of 8bits unsigned-int from the result of the RIPEMD-160 hash
bech32_address = encode('bc', 0, int_list_8based)
print('bech32_address:', bech32_address)

# print(bech32_hrp_expand('bc'))

