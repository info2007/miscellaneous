from bech32_toolbox_fr import *

wif_privatekey_demo = 'KyzPdqUeP3DVNVLVhTYpKsJvvCgcUJUk84fxXYB76yTrpQynfZSp'
wif_address_bech32_demo = 'bc1qyyg4f4mjx6axwesjl66z9skvqe88g2cyfrm7lc'

wif_privatekey = wif_privatekey_demo
wif_address = wif_address_bech32_demo

print('cle privee, WIF export format:', wif_privatekey)
# cle privee, WIF export format: KyzPdqUeP3DVNVLVhTYpKsJvvCgcUJUk84fxXYB76yTrpQynfZSp

# Du WIF a la cle prive:
# Source: https://en.bitcoin.it/wiki/Wallet_import_format
# Decode en utilisant base58check:
tmp1 = decoder_chaine_base58check_vers_chaine_hexadecimale(wif_privatekey)
# Retire le checksum a la fin
tmp2 = tmp1[:-8]
# Retire le premier octet (format)
tmp3 = tmp2[2:]
# Retire le dernier octet dans certain cas
if wif_privatekey[0] in 'KLM' and tmp3[-2:] == '01':
    tmp4 = tmp3[:-2]
else:
    tmp4 = tmp3

print('cle privee:', tmp4)
# cle privee: 52a8f451ed5a30fce81af75ece4a18661cb3d750a5efe1ac1ca74bdafd2d8452

# de la cle privee a la cle publique:
sk = ecdsa.SigningKey.from_string(bytearray.fromhex(tmp4), curve=ecdsa.SECP256k1)
vk = sk.verifying_key
tmp5 = vk.to_string("compressed").hex();

print('cle publique:', tmp5, ' (compressed)')

# Ci dessous tous les resultats intermediaires sur la cle publique de demonstration pour determiner l'adresse bech32.
# Chaque resulat est obtenu utilsant une function definie dans le fichier bech32_toolbox_fr.py
# Le but du jeu et de retrouver l'ordre d'appel de ces functions et de l'executer sur la cle publique donne au debut du
# jeu et ainsi de retrouver l'adresse bech32 correspondante.


# cle publique (compressee): 03d198766d52681f3fb35bfdf6ea48a48622cdf1099b9b69d55493d8ccf2adb82e
# hachage sha256: f05dbdc5a2d6dd1b616094733e2cb244d267166c4a15fca757f7692bacb2cd6b
# hachage ripemd160: 211154d77236ba676612feb422c2cc064e742b04
# liste_hexadecimale= ['2', '1', '1', '1', '5', '4', 'd', '7', '7', '2', '3', '6', 'b', 'a', '6', '7', '6', '6', '1', '2', 'f', 'e', 'b', '4', '2', '2', 'c', '2', 'c', 'c', '0', '6', '4', 'e', '7', '4', '2', 'b', '0', '4']
# liste_binaire= ['0010', '0001', '0001', '0001', '0101', '0100', '1101', '0111', '0111', '0010', '0011', '0110', '1011', '1010', '0110', '0111', '0110', '0110', '0001', '0010', '1111', '1110', '1011', '0100', '0010', '0010', '1100', '0010', '1100', '1100', '0000', '0110', '0100', '1110', '0111', '0100', '0010', '1011', '0000', '0100']
# chaine_binaire= 0010000100010001010101001101011101110010001101101011101001100111011001100001001011111110101101000010001011000010110011000000011001001110011101000010101100000100
# list binaire(5)= ['00100', '00100', '01000', '10101', '01001', '10101', '11011', '10010', '00110', '11010', '11101', '00110', '01110', '11001', '10000', '10010', '11111', '11010', '11010', '00010', '00101', '10000', '10110', '01100', '00000', '11001', '00111', '00111', '01000', '01010', '11000', '00100']
# liste_decimale: [4, 4, 8, 21, 9, 21, 27, 18, 6, 26, 29, 6, 14, 25, 16, 18, 31, 26, 26, 2, 5, 16, 22, 12, 0, 25, 7, 7, 8, 10, 24, 4]
# V+liste_decimale: [0, 4, 4, 8, 21, 9, 21, 27, 18, 6, 26, 29, 6, 14, 25, 16, 18, 31, 26, 26, 2, 5, 16, 22, 12, 0, 25, 7, 7, 8, 10, 24, 4]
# somme_de_controle: [9, 3, 27, 30, 31, 24]
# combination donnee+total: [0, 4, 4, 8, 21, 9, 21, 27, 18, 6, 26, 29, 6, 14, 25, 16, 18, 31, 26, 26, 2, 5, 16, 22, 12, 0, 25, 7, 7, 8, 10, 24, 4, 9, 3, 27, 30, 31, 24]
# base32-encodage: ['q', 'y', 'y', 'g', '4', 'f', '4', 'm', 'j', 'x', '6', 'a', 'x', 'w', 'e', 's', 'j', 'l', '6', '6', 'z', '9', 's', 'k', 'v', 'q', 'e', '8', '8', 'g', '2', 'c', 'y', 'f', 'r', 'm', '7', 'l', 'c']
# base32-chaine: qyyg4f4mjx6axwesjl66z9skvqe88g2cyfrm7lc
# bech32 address final: bc1qyyg4f4mjx6axwesjl66z9skvqe88g2cyfrm7lc
