""" Toolbox to generate Bech32 address
 from ECDSA private key. (function names in french)
"""

import base58
import ecdsa
import hashlib

# The reference implementation of bech32 from Pieter Wuille
from segwit_addr import *


def assembler_liste_vers_chaine(donnee):
    """Concatener tous les elements d'une liste en une chaine"""
    resultat = ''.join([element for element in donnee])
    return resultat


def calculer_hachage_ripemd160_chaine_hexadecimale(donnee):
    resultat = hashlib.new('ripemd160', bytearray.fromhex(donnee))
    return resultat.hexdigest()


def calculer_hachage_sha256_chaine_hexadecimale(donnee):
    resultat = hashlib.new('sha256', bytearray.fromhex(donnee))
    return resultat.hexdigest()


def calculer_somme_de_controle_bech32(donnee):
    resultat = bech32_create_checksum('bc', donnee, Encoding.BECH32)
    return resultat


def convertir_liste_binaire_vers_liste_decimale(donnee):
    resultat = [int(element, 2) for element in donnee]
    return resultat


def convertir_liste_binaire_vers_liste_hexadecimale(donnee):
    resultat = ['{0:02x}'.format(int(element, 2)) for element in donnee]
    return resultat


def convertir_liste_decimale_vers_liste_binaire(donnee):
    resultat = ["{0:04b}".format(element) for element in donnee]
    return resultat


def convertir_liste_decimale_vers_liste_hexadecimale(donnee):
    resultat = ["{0:02x}".format(element) for element in donnee]
    return resultat


def convertir_liste_hexadecimale_vers_liste_binaire(donnee):
    resultat = ["{0:04b}".format(int(chaine_hexa, base=16)) for chaine_hexa in donnee]
    return resultat


def convertir_liste_hexadecimale_vers_liste_decimale(donnee):
    resultat = [int(element, 16) for element in donnee]
    return resultat


def decoder_chaine_base58check_vers_chaine_hexadecimale(donnee):
    resultat = base58.b58decode(donnee).hex()
    return resultat


def encoder_liste_decimale_vers_liste_caracteres_bech32(donnee):
    resultat = [CHARSET[element] for element in donnee]
    return resultat


def trancher_chaine_vers_liste(chaine, taille):
    """Trancher une chaine de caractere vers une liste de chaines plus petites de taille <taille>"""
    resultat = [chaine[0 + i:taille + i] for i in range(0, len(chaine), taille)]
    return resultat


